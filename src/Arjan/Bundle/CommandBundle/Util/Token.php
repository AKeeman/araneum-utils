<?php
namespace Arjan\Bundle\CommandBundle\Util;

use Arjan\Bundle\CommandBundle\Exception\InvalidTokenException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Ramsey\Uuid\Uuid;

/**
 * Class Token
 * Token related utilities
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
final class Token
{
    /**
     * Create a new marcompro cloud compatible token
     * @param string $key
     * @param string $personId
     * @param string $fullName
     * @param string $email
     * @param \DateTime     $expireTime
     * @param \DateTime     $iat
     * @param bool     $once
     * @return string
     */
    public static function MCPEncode(string $key, string $personId, string $fullName, string $email, \DateTime $expireTime, \DateTime $iat = null, bool $once = false) : string
    {
        //payload
        $payload = [
            'iss'   => 'marcompro:' . $personId,
            'email' => $email,
            'name'  => $fullName,
        ];

        return self::encode($payload, $key, $expireTime, $iat, $once);
    }

    /**
     * encode payload
     * @param array          $payload
     * @param string         $key
     * @param \DateTime      $expireTime
     * @param \DateTime|null $iat
     * @param bool           $once
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function encode(array $payload, string $key, \DateTime $expireTime, \DateTime $iat = null, bool $once = false) : string
    {
        $iat = $iat ? $iat->getTimestamp() : time();
        $exp = $expireTime->getTimestamp();

        if ($exp < $iat) {
            throw new \InvalidArgumentException('The expire time should be in the future.');
        }

        $payload = array_merge($payload, [
            'iat' => $iat,
            'exp' => $exp,
        ]);

        if ($once) {
            $payload['jti'] = Uuid::uuid4();
        }

        return JWT::encode($payload, $key);
    }
    
    /**
     * Extract token data, no key required.
     * @param string $token
     * @return array
     * @throws InvalidTokenException
     */
    public static function decode(string $token) : array
    {
        $parts = explode('.', $token);
        if (3 !== count($parts)) {
            throw new InvalidTokenException($token);
        }

        list($header, $body, $hash) = $parts;

        $header_b64 = base64_decode($header);
        $header_decoded = json_decode($header_b64, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new InvalidTokenException($token);
        }

        $body_b64 = base64_decode($body);
        $body_decoded = json_decode($body_b64, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new InvalidTokenException($token);
        }

        foreach ($body_decoded as &$v) {
            if (is_int($v) && 1e9 < $v && 1e10 > $v) {
                $v = new \DateTime('@'.$v);
            }
        }

        return ['header' => $header_decoded, 'body' => $body_decoded, 'hash' => $hash];
    }

    /**
     * Check if the token is a validly signed token.
     * @param string $token
     * @param string $key
     * @return bool
     */
    public static function verify(string $token, string $key) : bool
    {
        try {
            JWT::decode($token, $key, ['HS256']);
            return true;

        } catch (\Exception $e) {
            if ($e instanceof ExpiredException || $e instanceof BeforeValidException) {
                return true;
            }
        }

        return false;
    }
}
