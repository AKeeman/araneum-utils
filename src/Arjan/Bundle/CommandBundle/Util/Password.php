<?php
namespace Arjan\Bundle\CommandBundle\Util;

/**
 * Class Password
 * Password related utilities
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
final class Password
{
    /**
     * Hash a mcp password
     * @param string $password
     * @return string
     */
    public static function hash(string $password) : string
    {
        return hash_hmac('sha256', $password, 'salt'.strlen($password));
    }
}
