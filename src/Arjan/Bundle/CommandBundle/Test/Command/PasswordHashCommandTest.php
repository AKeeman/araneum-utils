<?php
namespace Arjan\Bundle\CommandBundle\Test\Command;

use Arjan\Bundle\CommandBundle\Command\PasswordHashCommand;
use araneumbv\CommandUtils\CommandTester;

/**
 * Class PasswordHashCommandTest
 * Test the password hash command
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 * @coversDefaultClass Arjan\Bundle\CommandBundle\Command\PasswordHashCommand
 */
class PasswordHashCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test if a provided password is hashed properly
     * @covers ::configure
     * @covers ::execute
     */
    public function testExecute()
    {
        $password = 'fooBar';
        $hash = '99a6e60c527dc0fd5359da6f2bad4fc6a23f93b7709eeebc4bcb0ff415baacbb';

        $commandTester = new CommandTester(new PasswordHashCommand());
        $commandTester->execute(['password' => $password], ['interactive' => false]);

        $this->assertRegExp(
            sprintf('/%s/', preg_quote($hash, '/')),
            $commandTester->getDisplay()
        );
    }

    /**
     * Interactive test
     * @covers ::interact
     */
    public function testInteractive()
    {
        $password = 'fooBar';
        $hash = '99a6e60c527dc0fd5359da6f2bad4fc6a23f93b7709eeebc4bcb0ff415baacbb';

        $commandTester = new CommandTester(new PasswordHashCommand());
        $commandTester->setInteractiveInput([$password]);
        $commandTester->execute();

        $this->assertEmpty($commandTester->getInputStreamContents());
        $this->assertRegExp(
            sprintf('/%s/', preg_quote($hash, '/')),
            $commandTester->getDisplay()
        );
    }
}
