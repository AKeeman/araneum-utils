<?php
namespace Arjan\Bundle\CommandBundle\Test\Command;

use Arjan\Bundle\CommandBundle\Command\CreateMcpTokenCommand;
use Arjan\Bundle\CommandBundle\Util\Token;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CreateMcpTokenCommandTest
 * Test the create mcp token command
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 * @coversDefaultClass Arjan\Bundle\CommandBundle\Command\CreateMcpTokenCommand
 */
class CreateMcpTokenCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test if a provided password is hashed properly
     * @covers ::configure
     * @covers ::execute
     */
    public function testExecute()
    {
        $command = new CreateMcpTokenCommand();
        $application = new Application();
        $application->add($command);

        $commandTester = new CommandTester($command);

        $commandTester->execute([
            'personId' => '123',
            'fullName' => 'Foo Bar',
            'email' => 'foo@bar.test',
            'key' => 'mySecretKey',
            'iat' => '3-1-2016 0:00:00',
            'exp' => '3-1-2016 23:59:59',
            '--once' => false,
        ], [
            'interactive' => false,
        ]);

        preg_match('/[0-9a-z]+\.[0-9a-z]+\.[0-9a-z_-]+/i', $commandTester->getDisplay(), $match);
        $token = $match[0];

        $data = Token::decode($token);
        $this->assertEquals('foo@bar.test', $data['body']['email'] ?? null);
    }

    //@todo interactive tests
}
