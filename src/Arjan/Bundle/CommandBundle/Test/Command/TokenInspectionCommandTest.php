<?php
namespace Arjan\Bundle\CommandBundle\Test\Command;

use Arjan\Bundle\CommandBundle\Command\TokenInspectionCommand;
use Arjan\Bundle\CommandBundle\Util\Token;
use araneumbv\CommandUtils\CommandTester;

/**
 * Class TokenInspectionCommandTest
 * Test the token inspection command
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 * @coversDefaultClass Arjan\Bundle\CommandBundle\Command\TokenInspectionCommand
 */
class TokenInspectionCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test if a token can be decoded without a key
     * @dataProvider dataProvider
     * @param string $keyArg
     * @param bool $validateKey
     * @covers ::configure
     * @covers ::execute
     * @covers ::decode
     * @covers ::testValidity
     */
    public function testExecute(string $keyArg = null, bool $validateKey = false)
    {
        $command = new TokenInspectionCommand();
        $commandTester = new CommandTester($command);

        $payload = [
            'foo' => 'bar',
        ];
        $key = 'verySecretKey';
        $exp = (new \DateTime())->modify('-23 hours');
        $iat = (new \DateTime())->modify('-24 hours');

        $token = Token::encode($payload, $key, $exp, $iat);

        $commandTester->execute([
            'token' => $token,
            'key' => $keyArg,
        ], [
            'interactive' => false,
        ]);

        $this->assertRegExp(
            '/(?<![a-z])foo(?![a-z]).+(?<![a-z])bar(?![a-z])/',
            $commandTester->getDisplay()
        );

        if ($validateKey) {
            $test = $keyArg === $key ? 'token is valid' : 'token is not valid';
            $this->assertRegExp(
                sprintf('/%s/', preg_quote($test, '/')),
                $commandTester->getDisplay()
            );

        } else {
            $this->assertNotRegExp('/valid/', $commandTester->getDisplay());
        }
    }

    /**
     * Provide test data
     * @return array
     */
    public function dataProvider() : array
    {
        return [
            [null, false],
            ['-', false],
            ['verySecretKey', true],
            ['wrongSecretKey', true],
        ];
    }

    //@todo interactive tests
}
