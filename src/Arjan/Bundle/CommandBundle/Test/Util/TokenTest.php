<?php
namespace Arjan\Bundle\CommandBundle\Test\Util;

use Arjan\Bundle\CommandBundle\Util\Token;

/**
 * Class TokenTest
 * Test the Token utility
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 * @coversDefaultClass Arjan\Bundle\CommandBundle\Util\Token
 */
class TokenTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the encode function, verify the result and compare decoded data to input,
     * for both a multi-use key as a once key. Also check verify errors on invalid keys.
     * @covers       ::encode
     * @covers       ::decode
     * @covers       ::verify
     * @dataProvider encodeDecodeVerifyProvider
     * @param \DateTime|null $iat
     * @param bool           $once
     */
    public function testEncodeDecodeVerify(\DateTime $iat = null, bool $once = false)
    {
        $key = 'verySecretKey';
        $payload = ['foo' => 'bar'];
        $exp = (new \DateTime())->modify('+1 day, +1 hour');

        $token = Token::encode($payload, $key, $exp, $iat, $once);
        $this->assertRegExp('/^[a-z0-9]+\.[a-z0-9]+\.[a-z0-9_-]+$/i', $token);

        $this->assertTrue(Token::verify($token, $key), 'token could not be validated');
        $this->assertFalse(Token::verify('x'.$token, $key), 'token should not be validated');
        $this->assertFalse(Token::verify($token, 'x'.$key), 'token should not be validated with this given key');

        $data = Token::decode($token);
        $this->assertArrayHasKey('header', $data);
        $this->assertArrayHasKey('body', $data);
        $this->assertArrayHasKey('hash', $data);
        $this->assertArrayHasKey('foo', $data['body']);
        $this->assertEquals('bar', $data['body']['foo']);

        if ($iat) {
            $this->assertEquals($iat, $data['body']['iat']);
        }

        if ($once) {
            $this->assertArrayHasKey('jti', $data['body']);
            $this->assertRegExp('/^[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$/', $data['body']['jti']);
        }
    }

    /**
     * Provide data for encode/decode/verify test
     * @return bool[]
     */
    public function encodeDecodeVerifyProvider() : array
    {
        return [
            [null, false],
            [null, true],
            [new \DateTime(), false],
            [new \DateTime(), true],
            [(new \DateTime())->modify('+1 day'), false],
        ];
    }

    /**
     * Iat is later than expire time
     * @covers ::encode
     * @expectedException \InvalidArgumentException
     */
    public function testIatGtExp()
    {
        $iat = (new \DateTime())->modify('+1 hour');
        $exp = new \DateTime();
        Token::encode(['foo' => 'bar'], 'somekey', $exp, $iat);
    }

    /**
     * Test the mcp token creation functionality.
     * @covers ::MCPEncode
     */
    public function testMCPEncodeDecodeVerify()
    {
        $key = 'otherVerySecretKey';
        $iat = new \DateTime();
        $exp = (new \DateTime())->modify('+1 hour');
        $personId = 'a6e6c88eac36e87c7e6aafc';
        $fullName = 'Foo Bar';
        $email = 'unit-test@foo.bar';

        $token = Token::MCPEncode($key, $personId, $fullName, $email, $exp, $iat, false);

        $data = Token::decode($token);
        $expected = [
            'iss'   => 'marcompro:'.$personId,
            'email' => $email,
            'name'  => $fullName,
            'iat'   => $iat,
            'exp'   => $exp,
        ];

        $this->assertEquals($expected, $data['body']);
    }
}
