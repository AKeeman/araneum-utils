<?php
namespace Arjan\Bundle\CommandBundle\Test\Util;

use Arjan\Bundle\CommandBundle\Util\Password;
use PHPUnit_Framework_TestCase;

/**
 * Class PasswordTest
 * Test the password util
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
class PasswordTest extends PHPUnit_Framework_TestCase
{
    /**
     * @covers Arjan\Bundle\CommandBundle\Util\Password::hash
     */
    public function testHash()
    {
        $hash = 'b90621e6dc297b28235374fb022595dee25bb52ee953f3e2f86386942c1efacd';
        $this->assertEquals($hash, Password::hash('testPassword'));
    }
}
