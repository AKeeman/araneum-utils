<?php
namespace Arjan\Bundle\CommandBundle\Command;

use Arjan\Bundle\CommandBundle\Util\Token;
use araneumbv\CommandUtils\QuestionValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class CreateMcpTokenCommand
 * Create a mcp compatible token
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
final class CreateMcpTokenCommand extends Command
{
    /**
     * Configure this command
     */
    protected function configure()
    {
        $this
            ->setName('token:create-mcp')
            ->setDescription('Create an mcp compatible token')
            ->addArgument(
                'personId',
                InputArgument::REQUIRED,
                'The mcp person id'
            )
            ->addArgument(
                'fullName',
                InputArgument::REQUIRED,
                'The full name of the person'
            )
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'The persons email address'
            )
            ->addArgument(
                'key',
                InputArgument::REQUIRED,
                'The key that should be used for hashing'
            )
            ->addArgument(
                'iat',
                InputArgument::OPTIONAL,
                'Token start time format (default: now)',
                'now'
            )
            ->addArgument(
                'exp',
                InputArgument::OPTIONAL,
                'Token expiration time format (default: +12 hours)',
                '+12 hours'
            )
            ->addOption(
                'once',
                'o',
                InputOption::VALUE_NONE,
                'Include a uuid4 in the token to use it once'
            );
    }

    /**
     * Interactively set the password, when not given
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // first, set up validators

        // normal question validator
        $validator = new QuestionValidator([new NotBlank()]);

        // email question validator
        $emailValidator = new QuestionValidator([new NotBlank(), new Email()]);

        // datetime question validator
        $dateFormatChecker = function (string $value = null, ExecutionContextInterface $context) {
            if (! @(new \DateTime())->modify($value)) {
                $context->buildViolation('The answer is not a proper datetime modify format.')
                    ->atPath($context->getPropertyPath())
                    ->addViolation();

                return false;
            }

            return true;
        };
        $dateFormatConstraint = new Callback(['callback' => $dateFormatChecker]);
        $dateValidator = new QuestionValidator([new NotBlank(), $dateFormatConstraint]);

        // datetime question validator with relation to $relToContainer
        $relToContainer = null;
        $relDateFormatChecker = function (string $value = null, ExecutionContextInterface $context) use (
            &$relToContainer,
            $dateFormatChecker
        ) {
            if (call_user_func($dateFormatChecker, $value, $context)) {
                $dateLow = (new \DateTime())->modify(($relToContainer));
                $dateHigh = (new \DateTime())->modify($value);
                if ($dateLow > $dateHigh) {
                    $context->buildViolation('The datetime should be later than the other one.')
                        ->atPath($context->getPropertyPath())
                        ->addViolation();
                }
            }
        };
        $relDateFormatConstraint = new Callback(['callback' => $relDateFormatChecker]);
        $relDateValidator = new QuestionValidator([new NotBlank(), $relDateFormatConstraint]);

        $forceAskOptional = false;

        // then, ask questions

        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');

        if (! $validator->isValid($input->getArgument('personId'))) {
            $question = new Question('The person id: ');
            $question->setValidator($validator->getCallback());
            $personId = $questionHelper->ask($input, $output, $question);
            $input->setArgument('personId', $personId);
        }

        if (! $validator->isValid($input->getArgument('fullName'))) {
            $question = new Question('The persons full name: ');
            $question->setValidator($validator->getCallback());
            $fullName = $questionHelper->ask($input, $output, $question);
            $input->setArgument('fullName', $fullName);
        }

        if (! $emailValidator->isValid($input->getArgument('email'))) {
            $question = new Question('The persons email: ');
            $question->setValidator($emailValidator->getCallback());
            $email = $questionHelper->ask($input, $output, $question);
            $input->setArgument('email', $email);
            $forceAskOptional = true;
        }

        if (! $validator->isValid($input->getArgument('key'))) {
            $question = new Question('The hashing key: ');
            $question->setValidator($validator->getCallback());
            $key = $questionHelper->ask($input, $output, $question);
            $input->setArgument('key', $key);
        }

        if ($forceAskOptional || ! $dateValidator->isValid($input->getArgument('iat'))) {
            $default = 'now';
            $question = new Question("The token start time (dateTime modify format) [$default]: ", $default);
            $question->setValidator($dateValidator->getCallback());
            $iat = $questionHelper->ask($input, $output, $question);
            $input->setArgument('iat', $iat);
        }

        // variable is used in relDateValidator
        $relToContainer = $input->getArgument('iat');

        if ($forceAskOptional || ! $relDateValidator->isValid($input->getArgument('exp'))) {
            $default = '+12 hours';
            $question = new Question("The token expire time (dateTime modify format) [$default]: ", $default);
            $question->setValidator($relDateValidator->getCallback());
            $exp = $questionHelper->ask($input, $output, $question);
            $input->setArgument('exp', $exp);
        }

        if ($forceAskOptional) {
            $question = new ConfirmationQuestion('Want to create a unique token? [y/n]: ', false);
            $once = $questionHelper->ask($input, $output, $question);
            $input->setOption('once', $once);
        }
    }

    /**
     * Command execution code
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $token = Token::MCPEncode(
            $input->getArgument('key'),
            $input->getArgument('personId'),
            $input->getArgument('fullName'),
            $input->getArgument('email'),
            (new \DateTime())->modify($input->getArgument('exp')),
            (new \DateTime())->modify($input->getArgument('iat')),
            $input->getOption('once')
        );

        $output->writeln(sprintf('Generated token: %s', $token));

        return 0;
    }
}
