<?php
namespace Arjan\Bundle\CommandBundle\Command;

use Arjan\Bundle\CommandBundle\Exception\InvalidTokenException;
use Arjan\Bundle\CommandBundle\Util\Token;
use araneumbv\CommandUtils\QuestionValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class TokenInspectionCommand
 * Inspect a given token
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
class TokenInspectionCommand extends Command
{
    /**
     * Configure this command
     */
    protected function configure()
    {
        $this
            ->setName('token:inspect')
            ->setDescription('Inspect a token')
            ->addArgument(
                'token',
                InputArgument::REQUIRED,
                'The token that needs inspection'
            )
            ->addArgument(
                'key',
                InputArgument::OPTIONAL,
                'Decryption key. Requested for checking if the token is valid.'
            );
    }

    /**
     * Interactively set the arguments, when not given
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');

        $validator = new QuestionValidator([new NotBlank()]);

        $forceAskOptional = false;

        if (! $validator->isValid($input->getArgument('token'))) {
            $question = new Question('The token to be inspected: ');
            $question->setValidator($validator->getCallback());
            $password = $questionHelper->ask($input, $output, $question);
            $input->setArgument('token', $password);

            $forceAskOptional = true;
        }

        if ($forceAskOptional) {
            $question = new Question('Decryption key (leave empty if you don\'t want to validate the key): ');
            $key = $questionHelper->ask($input, $output, $question);
            if ($key) {
                $input->setArgument('key', $key);
            }
        }
    }

    /**
     * Command execution code
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $key = $input->getArgument('key');
        $key = '-' !== $key ? $key : null;

        $token = $input->getArgument('token');

        if ($key) {
            $this->testValidity($output, $token, $key);

            $output->writeln('');
        }

        $this->decode($output, $token);

        return 0;
    }

    /**
     * Test the validity of the token
     * @param OutputInterface $output
     * @param string          $token
     * @param string          $key
     */
    protected function testValidity(OutputInterface $output, string $token, string $key)
    {
        $validity = Token::verify($token, $key);

        $msg = $validity
            ? '<info>The token is valid.</info>'
            : '<error>The token is not valid.</error>';
        $output->writeln('Validity: '.$msg);
    }

    /**
     * Decode the token and show its contents
     * @param OutputInterface $output
     * @param string          $token
     * @throws InvalidTokenException
     */
    protected function decode(OutputInterface $output, string $token)
    {
        try {
            $data = Token::decode($token);

            $table = new Table($output);
            $table->setStyle('borderless');

            $table->setHeaders(['key', 'value']);

            foreach ($data['header'] as $k => $v) {
                $table->addRow([$k, $v]);
            }
            $table->addRow(new TableSeparator());
            foreach ($data['body'] as $k => $v) {
                if ($v instanceof \DateTime) {
                    $v = $v->format('Y-m-d H:i:s');
                }
                $table->addRow([$k, $v]);
            }
            $table->addRow(new TableSeparator());
            $table->addRow(['hash', $data['hash']]);

            $table->render();

        } catch (InvalidTokenException $e) {
            $output->writeln('<error>The token could not be decoded.</error>');
        }
    }
}
