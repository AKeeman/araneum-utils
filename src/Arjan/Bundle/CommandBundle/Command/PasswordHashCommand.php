<?php
namespace Arjan\Bundle\CommandBundle\Command;

use Arjan\Bundle\CommandBundle\Util\Password;
use araneumbv\CommandUtils\QuestionValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class PasswordHashCommand
 * Hash a password
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
final class PasswordHashCommand extends Command
{
    /**
     * Configure this command
     */
    protected function configure()
    {
        $this
            ->setName('password:hash')
            ->setDescription('Generate a mcp password hash')
            ->addArgument(
                'password',
                InputArgument::REQUIRED,
                'The password that has to be hashed'
            )
        ;
    }

    /**
     * Interactively set the password, when not given
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $validator = new QuestionValidator([new NotBlank()]);
        if (! $validator->isValid($input->getArgument('password'))) {
            /** @var QuestionHelper $questionHelper */
            $questionHelper = $this->getHelper('question');
            $question = new Question('The password to be hashed: ');
            $question->setValidator($validator->getCallback());
            $password = $questionHelper->ask($input, $output, $question);
            $input->setArgument('password', $password);
        }
    }

    /**
     * Command execution code
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln(sprintf('Hash: %s', Password::hash($input->getArgument('password'))));

        return 0;
    }
}
