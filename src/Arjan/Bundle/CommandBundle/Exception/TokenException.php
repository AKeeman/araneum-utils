<?php
namespace Arjan\Bundle\CommandBundle\Exception;

use Exception;

/**
 * Class TokenException
 * Use when something is wrong with a token
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
class TokenException extends Exception
{
    /**
     * The contextual token
     * @var string
     */
    private $token;

    /**
     * TokenException constructor.
     * @param string         $token
     * @param int            $code
     * @param Exception|null $previous
     */
    public function __construct($token, $code = 0, Exception $previous = null)
    {
        $this->setMessage($token);

        parent::__construct($this->message, $code, $previous);
    }

    /**
     * Set a message
     * @param $token
     */
    protected function setMessage($token)
    {
        $this->message = sprintf('Token exception with token "%s"', $token);
    }

    /**
     * Get Token
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
