<?php
namespace Arjan\Bundle\CommandBundle\Exception;

/**
 * Class TokenExpiredException
 * Use when a token has been expired
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
class TokenExpiredException extends TokenException
{
    /**
     * Set a message
     * @param $token
     */
    protected function setMessage($token)
    {
        $this->message = sprintf('Token expired "%s"', $token);
    }
}
