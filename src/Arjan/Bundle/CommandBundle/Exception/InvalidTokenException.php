<?php
namespace Arjan\Bundle\CommandBundle\Exception;

/**
 * Class InvalidTokenException
 * Use when an invalid token is used
 *
 * @author Arjan Keeman <arjan@araneum.nl>
 */
class InvalidTokenException extends TokenException
{
    /**
     * Set a message
     * @param $token
     */
    protected function setMessage($token)
    {
        $this->message = sprintf('Invalid token "%s"', $token);
    }
}
